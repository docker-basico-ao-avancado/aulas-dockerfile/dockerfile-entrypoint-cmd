# Dockerfile - ENTRYPOINT e CMD

## Contexto

Este repositório foi criado como conteúdo adicional para a aula [**Dockerfile - ENTRYPOINT e CMD**](https://www.udemy.com/course/docker-do-basico-ao-avancado/learn/lecture/34071430#overview) do curso [**Docker - Básico ao Avançado**](https://www.udemy.com/course/docker-do-basico-ao-avancado/?couponCode=DOCKER_DEZ24), por Daniel Gil.

## Documentação Usada na Aula

- [Dockerfile - ENTRYPOINT](https://docs.docker.com/engine/reference/builder/#entrypoint)
- [Dockerfile - CMD](https://docs.docker.com/engine/reference/builder/#cmd)

## Conheça os Cursos

> Quer conhecer um dos cursos?
>
> Aqui você encontra links com cupons de desconto para:

**Docker - Básico ao Avançado**

[![Docker - Básico ao Avançado](https://danielgilcursos.blob.core.windows.net/images/docker-basico-ao-avancado.png)](https://www.udemy.com/course/docker-do-basico-ao-avancado/?couponCode=DOCKER_DEZ24)

**Terraform - Básico ao Avançado**

[![Terraform - Básico ao Avançado](https://danielgilcursos.blob.core.windows.net/images/terraform-basico-ao-avancado.png)](https://www.udemy.com/course/terraform-do-basico-ao-avancado/?couponCode=TERRAFORM_DEZ24)
